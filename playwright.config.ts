import { PlaywrightTestConfig } from '@playwright/test';

const config: PlaywrightTestConfig = {
    // Timeout
    timeout: 10000,
    /* Run tests in files in parallel */
    fullyParallel: true,
    /* Fail the build on CI if you accidentally left test.only in the source code. */
    forbidOnly: !!process.env.CI,
    /* Retry on CI only */
    retries: process.env.CI ? 2 : 0,
    /* Opt out of parallel tests on CI. */
    workers: process.env.CI ? 1 : undefined,
    /* Reporter to use. See https://playwright.dev/docs/test-reporters */
    reporter: 'list',
    use: {
        // Browser options
        headless: false,

        // Context options
        viewport: { width: 1280, height: 720 },

        // Artifacts
        screenshot: 'only-on-failure',
    },

    projects: [
        // {
        //   name: 'Chrome',
        //   use: { browserName: 'chromium' },
        // },
        {
            name: 'Firefox',
            use: { browserName: 'firefox' },
        }//,
        // {
        //   name: 'WebKit',
        //   use: { browserName: 'webkit' },
        // },
    ],
};

export default config;
